from flask import Flask
from flask import jsonify, request, Response
from flask_cors import CORS
from crontab import CronTab

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/cronjob", methods=['GET'])
def setCronjob():
    cron = CronTab(user=True)
    for job in cron:
        # cron.remove(job)
        print(job)

    job = cron.new(command='python /path/to/crontab-example/script.py')
    job.minute.every(1)
    # job.hour.on(12)
    cron.write()
    return 'True'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
