# Crontab Example
This is an example on utilizing crontab package with flask on Python

### Running the application
* Run with `$ python app.py`
* Check the cron job with `$ crontab -e`